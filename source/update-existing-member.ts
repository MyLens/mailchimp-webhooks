import LensReference from "./types";
import { resolveLens, memberExists, rectifyMergeFields } from './helpers';
import { updateMember, listMembers, listMergeFields, deleteMember } from '@mylens/mailchimp-integration';
import config from './config'; 
import createNewMember from "./create-new-member";

async function updateExistingMember(lensRef: LensReference) : Promise<void>{
    console.log('Received an update from a Lens!',  lensRef);
    let resolvedLens = await resolveLens(lensRef);  
    console.log('Resolved lens = ', resolvedLens); 
    let email = resolvedLens.find(x => x.name.toLowerCase() === "emailaddress").value; 
    if (!email) {
        throw new Error('Mailchimp requires an email, refusing to update.')
    }

    // TODO: Need to look for lenses instead. 
    let exists = await memberExists(email);
    if(!exists) {
        console.log('member does not exist...'); 
        // Check for existing members
        let members = (await listMembers(config.MAILCHIMP_LIST_ID)).members
        let mergeFields = (await listMergeFields(config.MAILCHIMP_LIST_ID)).merge_fields;
        let nameToTag = mergeFields.reduce((acc, curr) => {
            acc[curr.name] = curr.tag;
            return acc;
        }, {})
    
        let member = members.find(m => {
            return m.merge_fields[nameToTag["lensID"]] === lensRef.id;
        })

        if (!member) {
            console.log(`No Lens with Lens ID = ${lensRef.id} exists in Mailchimp audience. Doing nothing...`); 
            return; 
        }

        if (member.email_address.toLowerCase() === email){
            console.log('Emails are the same, doing nothing!'); 
            return; 
        }

        // Delete old email address: 
        await deleteMember(config.MAILCHIMP_LIST_ID, member.email_address); 
        console.log('Deleted old email address.')
        await createNewMember(lensRef); 
        console.log('Finished creating a new member.')
        return ; 
        // throw new Error(`Email "${email} does NOT exist in Mailchimp, refusing to update...`); 
    }

    let mergeField = await rectifyMergeFields(resolvedLens); 
    let updatedUserObject = {
        merge_fields: mergeField, 
        email_address: email, 
    }
    let updatedUser = await updateMember(config.MAILCHIMP_LIST_ID, updatedUserObject); 
    console.log('Succesfully updated user: ', updatedUser); 
} 

export default updateExistingMember; 