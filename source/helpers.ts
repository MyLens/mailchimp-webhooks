import LensApi from '@mylens/lens-api'; 

import { createMergeField, getMember, listMergeFields, sync } from '@mylens/mailchimp-integration';
import Async from 'async';
import config from './config';
import LensReference, { ResolvedItem, MergeField } from './types';

export const api = new LensApi({ clientId: config.LENS_ENTERPRISE_PROJECT_ID, privateKey: config.LENS_ENTERPRISE_PRIVATE_KEY });

/**
 * Make sure all the merge fields that are necessary are present in the Resolved Lens. Create mergefields if they are not present, 
 * return ALL the merge fields needed to create this particular user. All fields are created that the user has in their lens. 
 * @param result 
 */
export async function rectifyMergeFields(result: Array<ResolvedItem>) : Promise<{[merge_field: string]: string}>{
    let mergeFields: Array<MergeField> = (await listMergeFields(config.MAILCHIMP_LIST_ID)).merge_fields.map(x => {
        return {
            merge_id: x.merge_id,
            tag: x.tag,
            name: x.name
        };
    });
    console.log('Current merge fields: ', mergeFields); 
    let missing = result.reduce((acc, curr) => {
        if (curr.name.toLowerCase() === 'emailaddress')
            return acc; // skip email 
        let f = mergeFields.find(x => x.name === curr.name);
        if (!f)
            acc.push(curr);
        return acc;
    }, []);
    if (missing.length > 0) {
        console.log('Some merge fields are missing, going to create them now...');
        console.log('missing fields: ', missing);
        let newFields = await createMissingFields(missing);
        mergeFields = mergeFields.concat(newFields);
    }
    else {
        console.log('No missing merge fields, going to create user.');
    }
    console.log('mergeFields = ', mergeFields);
    let mergeField = result.reduce((acc, curr) => {
        let f = mergeFields.find(x => {
            if (!x)
                return false;
            return x.name === curr.name;
        });
        if (!f)
            return acc;
        acc[f.tag] = curr.value;
        return acc;
    }, {});
    return mergeField;
}

async function createMissingFields(missingFields: Array<any>): Promise<Array<MergeField>> {
    // If I don't rate limit, I get a "Bad Request" error from the MailChimp API. 
    let q = Async.queue(async (x) => {
        return createMergeField(config.MAILCHIMP_LIST_ID, x, 'text');
    }, 2);
    let mergeFields = await Promise.all(missingFields.map(async (data) => {
        if (data.name !== "emailAddress") {
            // create merge fields with name and always use text for now. 
            return q.push(data.name);
        }
    }));
    console.log('Add new fields: ', mergeFields);
    let fields: Array<MergeField> = mergeFields.map((x): MergeField => {
        return {
            merge_id: x.merge_field,
            name: x.name,
            tag: x.tag
        };
    });
    return fields;
}
/**
 * Given an email, return true or false if this user exists. 
 * @param email 
 */
export async function memberExists(email: any) {
    let memberExists = false;
    try {
        let member = await getMember(config.MAILCHIMP_LIST_ID, email);
        if (member && (member.status==='subscribed')) {
            memberExists = true;
        }
    } catch (e) {
       
    }
    
    return memberExists;
}


/**
 * Attempt to decrypt and organize the data in a Lens using the NameMap and api. 
 * @param lensRef 
 */
export async function resolveLens(lensRef: LensReference): Promise<Array<ResolvedItem>> {
    let result;
    // Check to see if the Lens contains the key to open it. 
    if (lensRef.target && lensRef.target.privateKey) {
        result = await api.resolveLens({ url: lensRef.url, privateKey: lensRef.target.privateKey });
    }
    else {
        // Try to use our private key to open if not. 
        result = await api.resolveLens({ url: lensRef.url, privateKey: config.LENS_ENTERPRISE_PRIVATE_KEY });
    }
    return sync.transform(result);
}
