
import {
    createMember
} from '@mylens/mailchimp-integration'; 

import config from './config'; 
import LensReference from './types'; 
import { resolveLens, memberExists, rectifyMergeFields } from './helpers';


async function createNewMember(lensRef: LensReference) : Promise<void>{
    console.log('Creating a new member!',  lensRef);
    let result = await resolveLens(lensRef);
    console.log('Resolved Lens = ', result); 


    let email = result.find(x => x.name.toLowerCase() === "emailaddress").value; 
    console.log('email address = ', email);  
    
    let exists = await memberExists(email); 
    if (exists) {
        console.log('The member already exists, skipping this add...'); 
        return; 
    }

    // fetch merge fields to make sure they exist: 
    let mergeField = await rectifyMergeFields(result);

    let newUserObject = {
        merge_fields: mergeField, 
        email_address: email, 
        status: 'subscribed'
    }

    console.log('newUserObject = ', newUserObject); 

    let newMember = await createMember(config.MAILCHIMP_LIST_ID, newUserObject); 
    console.log('Successfully created new member! ', newMember); 
    return; 
}

export default createNewMember; 
