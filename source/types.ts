export default interface LensReference {
    id: string,
    url: string,
    isSelfTarget: boolean,
    target?: LensTarget,
    state?: string,
    created: number
    ownerId: string,
    isRevoked: boolean
}

export interface LensTarget {
    id: string,
    publicKey?: string,
    privateKey?: string
}

export interface ResolvedItem {
    name: string, 
    value: string
}

export interface MergeField {
    merge_id: string, 
    tag: string, 
    name: string, 
}
