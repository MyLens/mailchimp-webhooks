import LensReference from "./types";
import config from './config';
import { listMembers, listMergeFields, deleteMember } from '@mylens/mailchimp-integration';


async function deleteExistingMember(lensRef: LensReference): Promise<void> {
    console.log('Received a delete request for lens', lensRef);
    let members = (await listMembers(config.MAILCHIMP_LIST_ID)).members
    console.log(`Found ${members.length} members. ` )
    let mergeFields = (await listMergeFields(config.MAILCHIMP_LIST_ID)).merge_fields;
    let nameToTag = mergeFields.reduce((acc, curr) => {
        acc[curr.name] = curr.tag;
        return acc;
    }, {})

    let member = members.find(m => {
        return m.merge_fields[nameToTag["lensID"]] === lensRef.id;
    })
    
    if (!member) {
        console.log(`Did not find the member with lens id: "${lensRef.id}"`); 
        return 
    }
    console.log('deleting member: ', member.email_address); 

    await deleteMember(config.MAILCHIMP_LIST_ID, member.id);
    console.log('Successfully deleted member: ', member.email_address); 
}

export default deleteExistingMember; 