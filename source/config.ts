const config = {
    LENS_ENTERPRISE_ENDPOINT: process.env.LENS_ENTERPRISE_API || "https://staging-enterprise-service.mylens.io",
    LENS_ENTERPRISE_API_SECRET_KEY: process.env.LENS_ENTERPRISE_API_SECRET_KEY, 
    LENS_ENTERPRISE_API_ACCESS_KEY: process.env.LENS_ENTERPRISE_API_ACCESS_KEY,
    LENS_ENTERPRISE_PROJECT_ID: process.env.LENS_ENTERPRISE_PROJECT_ID || "",
    LENS_ENTERPRISE_PRIVATE_KEY: process.env.LENS_ENTERPRISE_PRIVATE_KEY || "",

    MAILCHIMP_LIST_ID: process.env.MAILCHIMP_LIST_ID, 
    MAILCHIMP_API_KEY: process.env.MAILCHIMP_API_KEY,
    MAILCHIMP_USERNAME: process.env.MAILCHIMP_USERNAME || "lensperson",
    MAILCHIMP_ENDPOINT: process.env.MAILCHIMP_ENDPOINT || "https://us20.api.mailchimp.com/3.0",
}
export default config; 