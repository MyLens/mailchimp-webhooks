import express from 'express';
import cors from 'cors';


import bodyParser from 'body-parser';
import { webhookMiddleware } from '@mylens/lens-enterprise-api';
import config from './source/config'; 

import createNewMember from './source/create-new-member'; 
import updateExistingMember from './source/update-existing-member'; 
import deleteExistingMember from './source/delete-existing-member'; 

const app = express();
app.use(cors()); 
app.use(bodyParser.json({ strict: false }));

app.post('/mailchimp-webhooks', webhookMiddleware(config.LENS_ENTERPRISE_PRIVATE_KEY), async (req, res) => {
    console.log("You found me!", req.header('X-Lens-Signature'));
    console.log(`Lens user "${req.body.lensRef.ownerId}" perfomed action "${req.body.action}" on their Lens!`)
    if (req.body.action == 'create'){
        await createNewMember(req.body.lensRef); 
    } else if (req.body.action == 'update'){
        await updateExistingMember(req.body.lensRef); 
    } else if (req.body.action == 'delete'){
        await deleteExistingMember(req.body.lensRef); 
    } else {
        throw new Error(`Received an unxpected lens action: "${req.body.action}"`)
    }
    res.status(200).send();
});

/* Error handling */
app.use((err, req, res, next) => {
    console.error("Error!!", err);
    if (!err.status) res.status(500).send({ message: "There was an internal error." });
    throw err;
});

export default app; 
