import 'source-map-support/register';
import serverless from 'serverless-http'; 
import ExpressServer from './server'; 

export const handler = serverless(ExpressServer); 
