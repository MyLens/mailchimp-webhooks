# Mailchimp Webhooks

This repo contains a serverless project that responds to webhooks fired when a 
user changes any data in their MyLens vault.